package com.gothasoftware.dn355.processor;

public interface ClockListener {

    void clockPrePulseInit();

    void clockRisingEdge();

    void clockFallingEdge();

}
