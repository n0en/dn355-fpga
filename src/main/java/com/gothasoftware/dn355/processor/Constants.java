package com.gothasoftware.dn355.processor;

@SuppressWarnings("OctalInteger")
public class Constants {
    public static final int MASK_18_BITS = 0777777;

    public static final int[] BIT_MASKS = {
            0,
            01,
            03,
            07,
            017,
            037,
            077,
            0177,
            0377,
            0777,
            01777,
            03777,
            07777,
            017777,
            037777,
            077777,
            0177777,
            0377777,
            0777777,
            01777777,
            03777777,
            07777777,
            017777777,
            037777777,
            077777777,
            0177777777,
            0377777777,
            0777777777,
    };

    public static final int MAX_MEMORY = 32767;

}
