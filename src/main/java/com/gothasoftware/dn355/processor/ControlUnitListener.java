package com.gothasoftware.dn355.processor;

public interface ControlUnitListener {

    void controlUnitMicroinstructionDecoded();

}
