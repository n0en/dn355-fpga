package com.gothasoftware.dn355.processor;

import java.util.LinkedList;
import java.util.List;

public class Signal implements ResetableComponent {

    private final String name;
    private final List<SignalListener> listeners = new LinkedList<>();
    private boolean signalAsserted;

    public Signal(String name) {
        this.name = name;
    }

    public void addListener(SignalListener listener) {
        listeners.add(listener);
    }

    public boolean isSignalAsserted() {
        return signalAsserted;
    }

    void assertSignal() {
        System.out.println("SIG: Assert " + name + " was " + signalAsserted);
        signalAsserted = true;
        listeners.forEach(listener -> listener.signalAsserted(name));
    }

    void releaseSignal() {
        System.out.println("SIG: Release " + name + " was " + signalAsserted);
        signalAsserted = false;
        listeners.forEach(listener -> listener.signalReleased(name));
    }

    @Override
    public void reset() {
        releaseSignal();
        System.out.println("SIG: Reset " + name);
    }

    public String getName() {
        return name;
    }
}
