package com.gothasoftware.dn355.processor;

public interface MemoryChangeListener {

    void memoryChanged(int address, long oldValue, long newValue);

}
