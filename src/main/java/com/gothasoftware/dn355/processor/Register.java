package com.gothasoftware.dn355.processor;

public interface Register {

    void addListener(RegisterListener listener);

    String getName();

    // Get the size of the register in bits
    int getSizeBits();

    int getValue();

    void setValue(int value);

}
