package com.gothasoftware.dn355.processor;

public class SimpleRegister extends AbstractRegister {

    public SimpleRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock) {
        super(name, bits, signalIn, signalOut, processor, clock, null, null);
    }

    public SimpleRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock, Signal signalZenable) {
        super(name, bits, signalIn, signalOut, processor, clock, signalZenable, null);
    }

    public SimpleRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock, Signal signalZenable, Signal signalNenable) {
        super(name, bits, signalIn, signalOut, processor, clock, signalZenable, signalNenable);
    }

}
