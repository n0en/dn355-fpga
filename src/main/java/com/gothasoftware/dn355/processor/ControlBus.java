package com.gothasoftware.dn355.processor;

import java.util.ArrayList;
import java.util.List;

public class ControlBus implements ResetableComponent, SignalListener {

    // Register data movement signals
    private final Signal signalIC_out;
    private final Signal signalIC_in;
    private final Signal signalMAR_in;
    private final Signal signalA_out;
    private final Signal signalA_in;
    private final Signal signalQ_out;
    private final Signal signalQ_in;
    private final Signal signalX1_out;
    private final Signal signalX1_in;
    private final Signal signalX2_out;
    private final Signal signalX2_in;
    private final Signal signalX3_out;
    private final Signal signalX3_in;
    private final Signal signalI_out;
    private final Signal signalI_in;
    private final Signal signalS_out;
    private final Signal signalS_in;

    private final Signal signalICTEMP_out;
    private final Signal signalICTEMP_in;

    private final Signal signalEA_out;
    private final Signal signalEA_in;

    private final Signal signalTAG_out;

    private final Signal signalMDR_out;
    private final Signal signalMDR_in;

    private final Signal signalIDR_in;
    private final Signal signalIDR_out;

    // Memory access signals
    private final Signal signalRead; // Initiate read of memory (memory[MAR] -> MDR)
    private final Signal signalWrite; // Initiate write of memory (MDR -> memory[MAR])

    // Function unit signals
    private final Signal signalIC_incr;
    private final Signal signalIndirect; // Op code indicates indirect memory reference
    private final Signal signalAdd; // IN register should add rather than load

    // Indicator update signals
    private final Signal signalZ_en; // Enable update of the Zero flag
    private final Signal signalN_en; // Enable update of the Negative flag

    private final List<ResetableComponent> resetableComponents = new ArrayList<>();

    private final List<SignalListener> allSignalListeners = new ArrayList<>();

    ControlBus() {
        signalIC_out = addSignal("IC_out");
        signalIC_in = addSignal("IC_in");
        signalMAR_in = addSignal("MAR_in");
        signalA_out = addSignal("A_out");
        signalA_in = addSignal("A_in");
        signalQ_out = addSignal("Q_out");
        signalQ_in = addSignal("Q_in");
        signalX1_out = addSignal("X1_out");
        signalX1_in = addSignal("X1_in");
        signalX2_out = addSignal("X2_out");
        signalX2_in = addSignal("X2_in");
        signalX3_out = addSignal("X3_out");
        signalX3_in = addSignal("X3_in");
        signalI_out = addSignal("I_out");
        signalI_in = addSignal("I_in");
        signalS_out = addSignal("S_out");
        signalS_in = addSignal("S_in");

        signalICTEMP_out = addSignal("ICTEMP_out");
        signalICTEMP_in = addSignal("ICTEMP_in");

        signalEA_out = addSignal("EA_out");
        signalEA_in = addSignal("EA_in");

        signalTAG_out = addSignal("TAG_out");

        signalMDR_out = addSignal("MDR_out");
        signalMDR_in = addSignal("MDR_in");

        signalIDR_in = addSignal("IDR_in");
        signalIDR_out = addSignal("IDR_out");

        signalRead = new Signal("READ"); // Initiate read of memory (memory[MAR] -> MDR)
        signalWrite = new Signal("WRITE"); // Initiate write of memory (MDR -> memory[MAR])

        signalIC_incr = addSignal("IC_incr");
        signalIndirect = addSignal("Indirect");
        signalAdd = addSignal("ADD");

        signalZ_en = addSignal("Z_en");
        signalN_en = addSignal("N_en");

    }

    private Signal addSignal(String name) {
        Signal result = new Signal(name);
        resetableComponents.add(result);
        result.addListener(this);
        return result;
    }

    public void addAllSignalListener(SignalListener listener) {
        allSignalListeners.add(listener);
    }

    public Signal getSignalIC_out() {
        return signalIC_out;
    }

    public Signal getSignalIC_in() {
        return signalIC_in;
    }

    public Signal getSignalIC_incr() {
        return signalIC_incr;
    }

    public Signal getSignalMAR_in() {
        return signalMAR_in;
    }

    public Signal getSignalA_out() {
        return signalA_out;
    }

    public Signal getSignalA_in() {
        return signalA_in;
    }

    public Signal getSignalQ_out() {
        return signalQ_out;
    }

    public Signal getSignalQ_in() {
        return signalQ_in;
    }

    public Signal getSignalX1_out() {
        return signalX1_out;
    }

    public Signal getSignalX1_in() {
        return signalX1_in;
    }

    public Signal getSignalX2_out() {
        return signalX2_out;
    }

    public Signal getSignalX2_in() {
        return signalX2_in;
    }

    public Signal getSignalX3_out() {
        return signalX3_out;
    }

    public Signal getSignalX3_in() {
        return signalX3_in;
    }

    public Signal getSignalI_out() {
        return signalI_out;
    }

    public Signal getSignalI_in() {
        return signalI_in;
    }

    public Signal getSignalS_out() {
        return signalS_out;
    }

    public Signal getSignalS_in() {
        return signalS_in;
    }

    public Signal getSignalICTEMP_out() {
        return signalICTEMP_out;
    }

    public Signal getSignalICTEMP_in() {
        return signalICTEMP_in;
    }

    public Signal getSignalEA_out() {
        return signalEA_out;
    }

    public Signal getSignalEA_in() {
        return signalEA_in;
    }

    public Signal getSignalTAG_out() {
        return signalTAG_out;
    }

    public Signal getSignalMDR_out() {
        return signalMDR_out;
    }

    public Signal getSignalMDR_in() {
        return signalMDR_in;
    }

    public Signal getSignalIDR_in() {
        return signalIDR_in;
    }

    public Signal getSignalIDR_out() {
        return signalIDR_out;
    }

    public Signal getSignalIndirect() {
        return signalIndirect;
    }

    public Signal getSignalRead() {
        return signalRead;
    }

    public Signal getSignalWrite() {
        return signalWrite;
    }

    public Signal getSignalAdd() {
        return signalAdd;
    }

    public Signal getSignalZ_en() {
        return signalZ_en;
    }

    public Signal getSignalN_en() {
        return signalN_en;
    }

    @Override
    public void reset() {
        resetableComponents.forEach(ResetableComponent::reset);
    }

    @Override
    public void signalAsserted(String name) {
        allSignalListeners.forEach(listener -> listener.signalAsserted(name));
    }

    @Override
    public void signalReleased(String name) {
        allSignalListeners.forEach(listener -> listener.signalReleased(name));
    }
}
