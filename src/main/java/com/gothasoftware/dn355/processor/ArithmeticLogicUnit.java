package com.gothasoftware.dn355.processor;

public class ArithmeticLogicUnit implements ClockListener {

    private final Processor processor;
//    private final Signal signalALU_add;

    public ArithmeticLogicUnit(Processor processor) {
        this.processor = processor;
//        this.signalALU_add = processor.getSignalALU_add();

        processor.getClock().addListener(this);
    }

    @Override
    public void clockPrePulseInit() {
        // Don't care
    }

    @Override
    public void clockRisingEdge() {
//        if (signalALU_add.isSignalAsserted()) {
//            long value = processor.getRegisterMDR().getValue() + processor.getDataPath();
//            processor.getRegisterTEMP().setValue(value);
//        }
    }

    @Override
    public void clockFallingEdge() {
        // Don't care
    }
}
