package com.gothasoftware.dn355.processor;

public interface IndicatorListener {

    void indicatorChanged(String name, boolean newValue);

}
