package com.gothasoftware.dn355.processor;

@SuppressWarnings("OctalInteger")
public enum OpCodes {

    // Group 0: Memory Reference Instructions
    MPF(001, 0),
    ADCX2(002, 0),
    LDX2(003, 0),
    LDAQ(004, 0),
    ADA(006, 0),
    LDA(007, 0),
    TSY(010, 0),
    STX2(013, 0),
    STAQ(014, 0),
    ADAQ(015, 0),
    ASA(016, 0),
    STA(017, 0),
    SZN(020, 0),
    DVF(021, 0),
    CMPX2(023, 0),
    SBAQ(024, 0),
    SBA(026, 0),
    CMPA(027, 0),
    LDEX(030, 0),
    CANA(031, 0),
    ANSA(032, 0),
    ANA(034, 0),
    ERA(035, 0),
    SSA(036, 0),
    ORA(037, 0),
    ADCX3(040, 0),
    LDX3(041, 0),
    ADCX1(042, 0),
    LDX1(043, 0),
    LDI(044, 0),
    TNC(045, 0),
    ADQ(046, 0),
    LDQ(047, 0),
    STX3(050, 0),
    STX1(053, 0),
    STI(054, 0),
    TOV(055, 0),
    STZ(056, 0),
    STQ(057, 0),
    CIOC(060, 0),
    CMPX3(061, 0),
    ERSA(062, 0),
    CMPX1(063, 0),
    TNZ(064, 0),
    TPL(065, 0),
    SBQ(066, 0),
    CMPQ(067, 0),
    STEX(070, 0),
    TRA(071, 0),
    ORSA(072, 0),
    TZE(074, 0),
    TMI(075, 0),
    AOS(076, 0),

    // Group 1: Non-memory Reference Instructions (group op-codes, not individual instructions)
    G1_A(012, 1),
    G1_B(022, 1),
    G1_C(052, 1),
    G1_D(073, 1),

    // Group 2: Non-memory Reference Instructions (group op-code, not individual instructions)
    G2(033, 2),

    ;

    private final int opCode;
    private final int group;    // 0 = memory reference, 1 = group 1 non-memory, 2 = group 2 non-memory

    OpCodes(int opCode, int group) {
        this.opCode = opCode;
        this.group = group;
    }

    public int getOpCode() {
        return opCode;
    }

    public int getGroup() {
        return group;
    }

    public static OpCodes findOpCode(int opCode) {
        for (OpCodes v : values()) {
            if (v.getOpCode() == opCode) {
                return v;
            }
        }
        throw new IllegalArgumentException("Failed to find operation code " + opCode);
    }
}
