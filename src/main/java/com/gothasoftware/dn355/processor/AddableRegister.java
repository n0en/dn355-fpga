package com.gothasoftware.dn355.processor;

public class AddableRegister extends SimpleRegister {

    private final Signal addSignal;

    public AddableRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock, Signal addSignal, Signal signalZenable) {
        super(name, bits, signalIn, signalOut, processor, clock, signalZenable);

        this.addSignal = addSignal;
    }

    @Override
    public void clockFallingEdge() {
        if ((signalIn != null) && signalIn.isSignalAsserted()) {
            if ((addSignal != null) && addSignal.isSignalAsserted()) {
                setValue(getProcessor().getRegisterDataPath().getValue() + getValue());
            } else {
                setValue(getProcessor().getRegisterDataPath().getValue());
            }
        }
    }

}
