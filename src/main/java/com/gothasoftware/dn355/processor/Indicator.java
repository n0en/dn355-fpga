package com.gothasoftware.dn355.processor;

import java.util.ArrayList;
import java.util.List;

public class Indicator implements RegisterListener {

    private final String name;
    private final int bitMask;
    private final Register register;

    private boolean isActive;

    private final List<IndicatorListener> listeners = new ArrayList<>();

    public Indicator(String name, int bitMask, SimpleRegister register) {
        this.name = name;
        this.bitMask = bitMask;
        this.register = register;

        register.addListener(this);
    }

    public void addListener(IndicatorListener listener) {
        listeners.add(listener);
    }

    public String getName() {
        return name;
    }

    public void setActive(boolean active) {
        register.setValue(register.getValue() & (~bitMask) | (active ? bitMask : 0));
    }

    @Override
    public void registerValueChanged(long oldValue, long newValue) {
        if ((oldValue & bitMask) == (newValue & bitMask)) {
            return;
        }

        isActive = (newValue & bitMask) != 0;

        listeners.forEach(listener -> listener.indicatorChanged(name, isActive));
    }

}
