package com.gothasoftware.dn355.processor;

/*
 * Control Store Micro Instruction Format
 *
 * IN - 6-bits:
 *   0: None
 *   1: IC
 *   2: MAR
 *   3: IDR
 *   4: MDR
 *   5: A
 *   6: Q
 *   7: X1
 *  10: X2
 *  11: X3
 *  12: I
 *  13: S
 *
 * OUT - 6-bits:
 *   0: None
 *   1: IC
 *   2: TEMP
 *   3: IDR
 *   4: MDR
 *   5: A
 *   6: Q
 *   7: X1
 *  10: X2
 *  11: X3
 *  12: I
 *  13: S
 *
 * IT - 6-bits:
 *   0: Register to Register
 *   1: Memory Read
 *   2: Memory Read w/PC Increment
 *   3: Memory Write
 *
 * NEXT - 9-bits:
 *   NEXT CSAR (ignored for IT = 5)
 *
 * |31 - 28|27 - 21|20 - 15|14 - 9|8 - 0|
 * +-------+-------+-------+------+-----+
 * | FLAGS |  IN   | OUT   | IT   | NEXT|
 * +-------+-------+-------+------+-----+
 */

@SuppressWarnings({"OctalInteger", "PointlessBitwiseExpression"})
public class ControlStore {

    // Valid FLAGS codes
    static final int FLAGS_MASK = 017000000000;
    static final int FLAGS_SHIFT = 27;
    static final int FLAGS_NONE = 000000000000;
    static final int FLAGS_ZERO = 001000000000;
    static final int FLAGS_ZERO_NEG = 002000000000;

    // Valid IN codes
    static final int IN_MASK = 0770000000;
    static final int IN_SHIFT = 21;
    static final int IN_NONE = 0000000000;
    static final int IN_IC = 0010000000;
    static final int IN_MAR = 0020000000;
    static final int IN_IDR = 0030000000;
    static final int IN_MDR = 0040000000;
    static final int IN_A = 0050000000;
    static final int IN_Q = 0060000000;
    static final int IN_X1 = 0070000000;
    static final int IN_X2 = 0100000000;
    static final int IN_X3 = 0110000000;
    static final int IN_I = 0120000000;
    static final int IN_S = 0130000000;
    static final int IN_ICTEMP = 0140000000;
    static final int IN_EA = 0150000000;

    // Valid OUT codes
    static final int OUT_MASK = 0007700000;
    static final int OUT_SHIFT = 15;
    static final int OUT_NONE = 0000000000;
    static final int OUT_IC = 0000100000;
    static final int OUT_TAG = 0000200000;
    static final int OUT_IDR = 0000300000; // This operation will only write the D (displacement field)
    static final int OUT_MDR = 0000400000;
    static final int OUT_A = 0000500000;
    static final int OUT_Q = 0000600000;
    static final int OUT_X1 = 0000700000;
    static final int OUT_X2 = 0001000000;
    static final int OUT_X3 = 0001100000;
    static final int OUT_I = 0001200000;
    static final int OUT_S = 0001300000;
    static final int OUT_ICTEMP = 0001400000;
    static final int OUT_EA = 0001500000;

    // Valid IT codes
    static final int IT_MASK = 0000077000;
    static final int IT_SHIFT = 9;
    static final int IT_R_TO_R = 0000000000; // Register to Register transfer
    static final int IT_READ = 0000001000; // Memory read
    static final int IT_READ_PCINCR = 0000002000; // Memory read with PC Increment
    static final int IT_WRITE = 0000003000; // Memory write
    static final int IT_GROUP_BRANCH = 0000004000; // Branch using the op code group branch table
    static final int IT_INDIRECT_BRANCH = 0000005000; // Branch to next if indirect flag in IDR is on, otherwise execute next instruction
    static final int IT_ADD = 0000006000; // Add to IN register
    static final int IT_OP_CODE_BRANCH = 0000007000; // Branch using the op code branch tables

    static final int NEXT_MASK = 0000000777;

    private final static int[] controlProgram = {
            // Instruction fetch
            OUT_IC | IN_MAR | IT_R_TO_R | 001,  // 000
            OUT_IC | IN_ICTEMP | IT_READ_PCINCR | 002,  //001
            OUT_MDR | IN_IDR | IT_R_TO_R | 003,  // 002
            OUT_NONE | IN_NONE | IT_GROUP_BRANCH | 000,  // 003  (next ignored due to branch)

            0, // 004

            // Group 0: Memory Reference
            IN_NONE | OUT_NONE | IT_INDIRECT_BRANCH | 000,  // 005  (branch if indirect is on, otherwise next op)
            IN_EA | OUT_TAG | IT_R_TO_R | 007,  // 006
            IN_EA | OUT_IDR | IT_ADD | 010,  // 007

            //TODO: Implement direct address modifications

            IN_NONE | OUT_NONE | IT_OP_CODE_BRANCH | 000,  // 010  (next ignored due to branch)

            // TRA
            IN_IC | OUT_EA | IT_R_TO_R | 000,  // 011

            // Not indirect

            0, // 012
            0, // 013
            0, // 014
            0, // 015
            0, // 016
            0, // 017
            0, // 020
            0, // 021
            0, // 022
            0, // 023
            0, // 024
            0, // 025
            0, // 026
            0, // 027
            0, // 030
            0, // 031
            0, // 032
            0, // 033
            0, // 034
            0, // 035
            0, // 036
            0, // 037

            // Start of Group 1 Non-memory Reference Instructions
            FLAGS_NONE | IN_NONE | OUT_NONE | IT_OP_CODE_BRANCH | 000,  // 040  (next ignored due to branch)

            // ILA (Immediate Load A)
            FLAGS_ZERO_NEG | IN_A | OUT_IDR | IT_R_TO_R | 000, // 041

            // ILQ (Immediate Load Q)
            FLAGS_ZERO | IN_Q | OUT_IDR | IT_R_TO_R | 000, // 042

            // IAA (Immediate Add A)
            FLAGS_ZERO_NEG | IN_A | OUT_IDR | IT_ADD | 000, // 043

            // IAQ (Immediate Add Q)
            FLAGS_ZERO_NEG | IN_Q | OUT_IDR | IT_ADD | 000, // 044

            0, // 045
            0, // 046
            0, // 047

            0, // 050
            0, // 051
            0, // 052
            0, // 053
            0, // 054
            0, // 055
            0, // 056
            0, // 057

            0, // 060
            0, // 061
            0, // 062
            0, // 063
            0, // 064
            0, // 065
            0, // 066
            0, // 067

            0, // 070
            0, // 071
            0, // 072
            0, // 073
            0, // 074
            0, // 075
            0, // 076
            0, // 077

            0, // 100
            0, // 101
            0, // 102
            0, // 103
            0, // 104
            0, // 105
            0, // 106
            0, // 107

            0, // 110
            0, // 111
            0, // 112
            0, // 113
            0, // 114
            0, // 115
            0, // 116
            0, // 117

            0, // 120
            0, // 121
            0, // 122
            0, // 123
            0, // 124
            0, // 125
            0, // 126
            0, // 127

            // Start of Group 2 Non-memory Reference Instructions
            IN_NONE | OUT_NONE | IT_OP_CODE_BRANCH | 000,  // 130  (next ignored due to branch)

            // CAQ
            FLAGS_ZERO_NEG | OUT_A | IN_Q | IT_R_TO_R | 000, // 131

            // CQA
            FLAGS_ZERO_NEG | OUT_Q | IN_A | IT_R_TO_R | 000, // 132

            // CAX1
            FLAGS_ZERO | OUT_A | IN_X1 | IT_R_TO_R | 000, // 133

            // CAX2
            FLAGS_ZERO | OUT_A | IN_X2 | IT_R_TO_R | 000, // 134

            // CAX3
            FLAGS_ZERO | OUT_A | IN_X3 | IT_R_TO_R | 000, // 135

            // CX1A
            FLAGS_ZERO | OUT_X1 | IN_A | IT_R_TO_R | 000, // 136

            // CX2A
            FLAGS_ZERO | OUT_X2 | IN_A | IT_R_TO_R | 000, // 137

            // CX3A
            FLAGS_ZERO | OUT_X3 | IN_A | IT_R_TO_R | 000, // 140

            0, // 141
            0, // 142
            0, // 143
            0, // 144
            0, // 145
            0, // 146
            0, // 147


    };

    private final static int[] opCodeGroupBranchTable = {
            0005,
            0040,
            0130,
    };

    private final static int[] group0OpCodeBranchTable = {

            0000, // 000 Invalid
            0000, // 001 MPF
            0000, // 002 ADCX2
            0000, // 003 LDX2
            0000, // 004 LDAQ
            0000, // 005 Invalid
            0000, // 006 ADA
            0000, // 007 LDA

            0000, // 010 TSY
            0000, // 011 Invalid
            0000, // 012 Invalid
            0000, // 013 STX2
            0000, // 014 STAQ
            0000, // 015 ADAQ
            0000, // 016 ASA
            0000, // 017 STA

            0000, // 020 SZN
            0000, // 021 DVF
            0000, // 022 Invalid
            0000, // 023 CMPX2
            0000, // 024 SBAQ
            0000, // 025 Invalid
            0000, // 026 SBA
            0000, // 027 CMPA

            0000, // 030 LDEX
            0000, // 031 CANA
            0000, // 032 ANSA
            0000, // 033 Invalid
            0000, // 034 ANA
            0000, // 035 ERA
            0000, // 036 SSA
            0000, // 037 ORA

            0000, // 040 ADCX3
            0000, // 041 LDX3
            0000, // 042 ADCX1
            0000, // 043 LDX1
            0000, // 044 LDI
            0000, // 045 TNC
            0000, // 046 ADQ
            0000, // 047 LDQ

            0000, // 050 STX3
            0000, // 051 Invalid
            0000, // 052 Invalid
            0000, // 053 STX1
            0000, // 054 STI
            0000, // 055 TOV
            0000, // 056 STZ
            0000, // 057 STQ

            0000, // 060 CIOC
            0000, // 061 CMPX3
            0000, // 062 ERSA
            0000, // 063 CMPX1
            0000, // 064 TNZ
            0000, // 065 TPL
            0000, // 066 SBQ
            0000, // 067 CMPQ

            0000, // 070 STEX
            0011, // 071 TRA
            0000, // 072 ORSA
            0000, // 073 Invalid
            0000, // 074 TZE
            0000, // 075 TMI
            0000, // 076 AOS
            0000, // 077 Invalid

    };

    private final static int[] group1OpCode12BranchTable = {
            0000, // 00 RIER
            0000, // 01 Invalid
            0000, // 02 Invalid
            0000, // 03 Invalid
            0000, // 04 RIA
            0000, // 05 Invalid
            0000, // 06 Invalid
            0000, // 07 Invalid
    };

    private final static int[] group1OpCode22BranchTable = {
            0000, // 00 IANA
            0000, // 01 IORA
            0000, // 02 ICANA
            0000, // 03 IERA
            0000, // 04 ICMPA
            0000, // 05 Invalid
            0000, // 06 Invalid
            0000, // 07 Invalid
    };

    private final static int[] group1OpCode52BranchTable = {
            0000, // 00 SIER
            0000, // 01 Invalid
            0000, // 02 Invalid
            0000, // 03 Invalid
            0000, // 04 SIC
            0000, // 05 Invalid
            0000, // 06 Invalid
            0000, // 07 Invalid
    };

    private final static int[] group1OpCode73BranchTable = {
            0000, // Invalid
            0000, // IACX1
            0000, // IACX2
            0000, // IACX3
            0042, // ILQ
            0044, // IAQ
            0041, // ILA
            0043, // IAA
    };

    private final static int[][] group2OpCode33BranchTable = {
            { // 00
                    0000, // 00 Invalid
                    0000, // 01 Invalid
                    0134, // 02 CAX2
                    0000, // 03 Invalid
                    0000, // 04 LLS
                    0000, // 05 LRS
                    0000, // 06 ALS
                    0000, // 07 ARS
            },
            { // 01
                    0000, // 00 Invalid
                    0000, // 01 Invalid
                    0000, // 02 Invalid
                    0000, // 03 Invalid
                    0000, // 04 NRML
                    0000, // 05 Invalid
                    0000, // 06 NRM
                    0000, // 07 Invalid
            },
            { // 02
                    0000, // 00 Invalid
                    0000, // 01 NOP
                    0136, // 02 CX1A
                    0000, // 03 Invalid
                    0000, // 04 LLR
                    0000, // 05 LRL
                    0000, // 06 ALR
                    0000, // 07 ARL
            },
            { // 03
                    0000, // 00 Invalid
                    0000, // 01 INH
                    0137, // 02 CX2A
                    0140, // 03 CX3A
                    0000, // 04 Invalid
                    0000, // 05 Invalid
                    0000, // 06 ALP
                    0000, // 07 Invalid
            },
            { // 04
                    0000, // 00 Invalid
                    0000, // 01 DIS
                    0133, // 02 CAX1
                    0135, // 03 CAX3
                    0000, // 04 Invalid
                    0000, // 05 Invalid
                    0000, // 06 QLS
                    0000, // 07 QRS
            },
            { // 05
                    0000, // 00 Invalid
                    0000, // 01 Invalid
                    0000, // 02 Invalid
                    0000, // 03 Invalid
                    0000, // 04 Invalid
                    0000, // 05 Invalid
                    0000, // 06 Invalid
                    0000, // 07 Invalid
            },
            { // 06
                    0000, // 00 Invalid
                    0000, // 01 Invalid
                    0000, // 02 Invalid
                    0131, // 03 CAQ
                    0000, // 04 Invalid
                    0000, // 05 Invalid
                    0000, // 06 QLR
                    0000, // 07 QRL
            },
            { // 07
                    0000, // 00 Invalid
                    0000, // 01 ENI
                    0000, // 02 Invalid
                    0132, // 03 CQA
                    0000, // 04 Invalid
                    0000, // 05 Invalid
                    0000, // 06 QLP
                    0000, // 07 Invalid
            },
    };

    public int fetch(int address) {
        return controlProgram[address];
    }

    public int decodeFlags(int address) {
        return (controlProgram[address] & FLAGS_MASK) >> FLAGS_SHIFT;
    }

    public int decodeIn(int address) {
        return (controlProgram[address] & IN_MASK) >> IN_SHIFT;
    }

    public int decodeOut(int address) {
        return (controlProgram[address] & OUT_MASK) >> OUT_SHIFT;
    }

    public int decodeIt(int address) {
        return (controlProgram[address] & IT_MASK) >> IT_SHIFT;
    }

    public int decodeNext(int address) {
        return controlProgram[address] & NEXT_MASK;
    }

    int getOpCodeGroupBranchAddress(int group) {
        return opCodeGroupBranchTable[group];
    }

    int getGroup0OpCodeBranchAddress(int opcode) {
        return group0OpCodeBranchTable[opcode];
    }

    int getGroup1OpCode12BranchAddress(int s1) {
        return group1OpCode12BranchTable[s1];
    }

    int getGroup1OpCode22BranchAddress(int s1) {
        return group1OpCode22BranchTable[s1];
    }

    int getGroup1OpCode52BranchAddress(int s1) {
        return group1OpCode52BranchTable[s1];
    }

    int getGroup1OpCode73BranchAddress(int s1) {
        return group1OpCode73BranchTable[s1];
    }

    int getGroup2OpCode33BranchAddress(int s1, int s2) {
        return group2OpCode33BranchTable[s1][s2];
    }

}
