package com.gothasoftware.dn355.processor;

public interface SignalListener {

    void signalAsserted(String name);

    void signalReleased(String name);

}
