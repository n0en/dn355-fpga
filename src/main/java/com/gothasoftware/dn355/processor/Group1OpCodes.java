package com.gothasoftware.dn355.processor;

@SuppressWarnings("OctalInteger")
public enum Group1OpCodes {

    IACX1(073, 01),
    IACX2(073, 02),
    IACX3(073, 03),
    ILQ(073, 04),
    IAQ(073, 05),
    ILA(073, 06),
    IAA(073, 07),

    IANA(022, 00),
    IORA(022, 01),
    ICANA(022, 02),
    IERA(022, 03),
    ICMPA(022, 04),

    RIER(012, 00),
    RIA(012, 04),

    SIER(052, 00),
    SIC(025, 04),
    ;

    private final int opCode;
    private final int s1;

    Group1OpCodes(int opCode, int s1) {
        this.opCode = opCode;
        this.s1 = s1;
    }

    public int getOpCode() {
        return opCode;
    }

    public int getS1() {
        return s1;
    }

    public static Group1OpCodes findOpCode(int opCode, int s1) {
        for (Group1OpCodes code : values()) {
            if ((code.opCode == opCode) && (code.s1 == s1)) {
                return code;
            }
        }
        throw new IllegalArgumentException("Failed to find operation code " + opCode + " (s1=" + s1 + ")");
    }
}
