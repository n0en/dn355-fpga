package com.gothasoftware.dn355.processor;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("OctalInteger")
public class ControlUnit implements ClockListener, ResetableComponent {

    private final ControlStore controlStore = new ControlStore();
    private final ControlBus controlBus;

    private final SimpleRegister registerCSAR;  // Control Store Address Register - points to the next control store micro-instruction
    private final InstructionDecodeRegister registerIDR;   // Decodes the current instruction for micro-code execution
    private final SimpleRegister registerMAR;   // Memory Address Register - points to a memory location for memory read/write
    private final SimpleRegister registerMDR;   // Memory Data Register - contains the data read from or to be written to memory


    private final List<ControlUnitListener> listeners = new LinkedList<>();

    private int currentMicroinstruction;
    private int currentFlags;
    private int currentIn;
    private int currentOut;
    private int currentIt;
    private int currentNext;

    ControlUnit(Processor processor) {
        this.controlBus = processor.getControlBus();

        Clock clock = processor.getClock();

        registerCSAR = new SimpleRegister("CSAR", 9, null, null, processor, clock);
        registerIDR = new InstructionDecodeRegister("IDR", controlBus.getSignalIDR_in(), controlBus.getSignalIDR_out(),
                processor, clock);
        registerMAR = new SimpleRegister("MAR", 15, controlBus.getSignalMAR_in(), null, processor, clock);
        registerMDR = new MemoryAccessRegister("MDR", 18, controlBus.getSignalMDR_in(), controlBus.getSignalMDR_out(),
                processor, clock, registerMAR, controlBus.getSignalRead(), controlBus.getSignalWrite(), processor.getMemory());

        clock.addListener(this);
    }

    public void addListener(ControlUnitListener listener) {
        listeners.add(listener);
    }

    public void reset() {
        currentMicroinstruction = 0;
        currentFlags = 0;
        currentIt = 0;
        currentIn = 0;
        currentOut = 0;
        currentNext = 0;
    }

    @Override
    public void clockPrePulseInit() {
        System.out.println("CU: ClockPrePulseInit");

        registerCSAR.setValue(currentNext);

        int csar = currentNext;

        // Fetch the current micro-instruction from the control store and decode it
        currentMicroinstruction = controlStore.fetch(csar);

        currentFlags = controlStore.decodeFlags(csar);
        currentIn = controlStore.decodeIn(csar);
        currentOut = controlStore.decodeOut(csar);
        currentIt = controlStore.decodeIt(csar);
        currentNext = controlStore.decodeNext(csar);

        System.out.println("CU: Start IN=" + currentIn + " (" + getIn(currentIn)
                + "), OUT=" + currentOut + " (" + getOut(currentOut)
                + "), IT=" + currentIt
                + ", FLAGS=" + currentFlags
                + ", NEXT=" + currentNext);

        listeners.forEach(ControlUnitListener::controlUnitMicroinstructionDecoded);

        //
        // Activate processor signals based on decoded micro-instruction
        //

        controlBus.getSignalZ_en().releaseSignal();
        controlBus.getSignalN_en().releaseSignal();

        switch (currentFlags) {
            case 0: // No flags
                break;
            case 1: // Zero Flag Enable
                controlBus.getSignalZ_en().assertSignal();
                break;
            case 2: // Zero & Negative Flags Enable
                controlBus.getSignalZ_en().assertSignal();
                controlBus.getSignalN_en().assertSignal();
                break;
            default:
                throw new RuntimeException("Unknown micro-instruction FLAGS value of '" + currentFlags + "'");
        }

        controlBus.getSignalIC_out().releaseSignal();
        controlBus.getSignalIDR_out().releaseSignal();
        controlBus.getSignalMDR_out().releaseSignal();
        controlBus.getSignalA_out().releaseSignal();
        controlBus.getSignalQ_out().releaseSignal();
        controlBus.getSignalX1_out().releaseSignal();
        controlBus.getSignalX2_out().releaseSignal();
        controlBus.getSignalX3_out().releaseSignal();
        controlBus.getSignalI_out().releaseSignal();
        controlBus.getSignalS_out().releaseSignal();
        controlBus.getSignalICTEMP_out().releaseSignal();
        controlBus.getSignalEA_out().releaseSignal();
        controlBus.getSignalTAG_out().releaseSignal();

        switch (currentOut) {
            case 0: // No signal
                break;
            case 1: // IC
                controlBus.getSignalIC_out().assertSignal();
                break;
            case 2: // Contents of the register indicated by the current TAG
                switch (registerIDR.getTag()) {
                    case 0:
                        controlBus.getSignalICTEMP_out().assertSignal();
                        break;
                    case 1:
                        controlBus.getSignalX1_out().assertSignal();
                        break;
                    case 2:
                        controlBus.getSignalX2_out().assertSignal();
                        break;
                    case 3:
                        controlBus.getSignalX3_out().assertSignal();
                        break;
                    default:
                        throw new RuntimeException("Unknown TAG field with value " + registerIDR.getTag());
                }
                controlBus.getSignalTAG_out().assertSignal();
                break;
            case 3: // IDR
                controlBus.getSignalIDR_out().assertSignal();
                break;
            case 4: // MDR
                controlBus.getSignalMDR_out().assertSignal();
                break;
            case 5: // A
                controlBus.getSignalA_out().assertSignal();
                break;
            case 6: // Q
                controlBus.getSignalQ_out().assertSignal();
                break;
            case 7: // X1
                controlBus.getSignalX1_out().assertSignal();
                break;
            case 8: // X1
                controlBus.getSignalX2_out().assertSignal();
                break;
            case 9: // X1
                controlBus.getSignalX3_out().assertSignal();
                break;
            case 10: // I
                controlBus.getSignalI_out().assertSignal();
                break;
            case 11: // S
                controlBus.getSignalS_out().assertSignal();
                break;
            case 12: // ICTEMP
                controlBus.getSignalICTEMP_out().assertSignal();
                break;
            case 13: // EA
                controlBus.getSignalEA_out().assertSignal();
                break;
            default:
                throw new RuntimeException("Unknown micro-instruction IN value of '" + currentIn + "'");
        }

        controlBus.getSignalIC_in().releaseSignal();
        controlBus.getSignalMAR_in().releaseSignal();
        controlBus.getSignalIDR_in().releaseSignal();
        controlBus.getSignalMDR_in().releaseSignal();
        controlBus.getSignalA_in().releaseSignal();
        controlBus.getSignalQ_in().releaseSignal();
        controlBus.getSignalX1_in().releaseSignal();
        controlBus.getSignalX2_in().releaseSignal();
        controlBus.getSignalX3_in().releaseSignal();
        controlBus.getSignalI_in().releaseSignal();
        controlBus.getSignalS_in().releaseSignal();
        controlBus.getSignalICTEMP_in().releaseSignal();
        controlBus.getSignalEA_in().releaseSignal();

        switch (currentIn) {
            case 0: // No signal
                break;
            case 1: // IC
                controlBus.getSignalIC_in().assertSignal();
                break;
            case 2: // MAR
                controlBus.getSignalMAR_in().assertSignal();
                break;
            case 3: // IDR
                controlBus.getSignalIDR_in().assertSignal();
                break;
            case 4: // MDR
                controlBus.getSignalMDR_in().assertSignal();
                break;
            case 5: // A
                controlBus.getSignalA_in().assertSignal();
                break;
            case 6: // Q
                controlBus.getSignalQ_in().assertSignal();
                break;
            case 7: // X1
                controlBus.getSignalX1_in().assertSignal();
                break;
            case 8: // X1
                controlBus.getSignalX2_in().assertSignal();
                break;
            case 9: // X1
                controlBus.getSignalX3_in().assertSignal();
                break;
            case 10: // I
                controlBus.getSignalI_in().assertSignal();
                break;
            case 11: // S
                controlBus.getSignalS_in().assertSignal();
                break;
            case 12: // ICTEMP
                controlBus.getSignalICTEMP_in().assertSignal();
                break;
            case 13: // EA
                controlBus.getSignalEA_in().assertSignal();
                break;
            default:
                throw new RuntimeException("Unknown micro-instruction IN value of '" + currentIn + "'");
        }

        controlBus.getSignalIC_incr().releaseSignal();
        controlBus.getSignalAdd().releaseSignal();
        controlBus.getSignalRead().releaseSignal();
        controlBus.getSignalWrite().releaseSignal();

        switch (currentIt) {
            case 0: // Register to Register
                break;
            case 1: // Memory Read
                controlBus.getSignalRead().assertSignal();
                break;
            case 2: // Memory Read with PC increment
                System.out.println("CU: MR with PC Inc");
                controlBus.getSignalRead().assertSignal();
                controlBus.getSignalIC_incr().assertSignal();
                break;
            case 3:
                controlBus.getSignalWrite().assertSignal();
                break;
            case 4: // Branch using the op code group branch table
                currentNext = controlStore.getOpCodeGroupBranchAddress(registerIDR.getGroup());
                break;
            case 5: // Branch if indirect flag is on otherwise execute next instruction
                if (!controlBus.getSignalIndirect().isSignalAsserted()) {
                    currentNext = csar + 1;
                }
                break;
            case 6: // Add data path to register
                controlBus.getSignalAdd().assertSignal();
                break;

            case 7: // Branch using opcode tables
                switch (registerIDR.getGroup()) {
                    case 0:
                        currentNext = controlStore.getGroup0OpCodeBranchAddress(registerIDR.getOpCode());
                        break;
                    case 1:
                        switch (registerIDR.getOpCode()) {
                            case 012:
                                currentNext = controlStore.getGroup1OpCode12BranchAddress(registerIDR.getS1());
                                break;
                            case 022:
                                currentNext = controlStore.getGroup1OpCode22BranchAddress(registerIDR.getS1());
                                break;
                            case 052:
                                currentNext = controlStore.getGroup1OpCode52BranchAddress(registerIDR.getS1());
                                break;
                            case 073:
                                currentNext = controlStore.getGroup1OpCode73BranchAddress(registerIDR.getS1());
                                break;
                            default:
                                throw new RuntimeException("Unknown group 1 op code value of '" + registerIDR.getOpCode() + "'");
                        }
                        break;
                    case 2:
                        currentNext = controlStore.getGroup2OpCode33BranchAddress(registerIDR.getS1(), registerIDR.getS2());
                        break;
                    default:
                        throw new RuntimeException("Unknown op code group value of '" + registerIDR.getGroup() + "'");
                }
                break;

            default:
                throw new RuntimeException("Unknown micro-instruction IT value of '" + currentIt + "'");
        }
    }

    @Override
    public void clockRisingEdge() {
        System.out.println("CU: ClockRisingEdge");
    }

    @Override
    public void clockFallingEdge() {
        System.out.println("CU: ClockFallingEdge");
    }

    public int getCurrentMicroinstruction() {
        return currentMicroinstruction;
    }

    public int getCurrentFlags() {
        return currentFlags;
    }

    public int getCurrentIn() {
        return currentIn;
    }

    public int getCurrentOut() {
        return currentOut;
    }

    public int getCurrentIt() {
        return currentIt;
    }

    public int getCurrentNext() {
        return currentNext;
    }

    public SimpleRegister getRegisterCSAR() {
        return registerCSAR;
    }

    public InstructionDecodeRegister getRegisterIDR() {
        return registerIDR;
    }

    public SimpleRegister getRegisterMAR() {
        return registerMAR;
    }

    public SimpleRegister getRegisterMDR() {
        return registerMDR;
    }

    public static String getOut(int out) {
        switch (out) {
            case 0: // No signal
                return "NONE";
            case 1: // IC
                return "IC";
            case 2: // Contents of current TAG register
                return "TAG";
            case 3: // IDR
                return "IDR";
            case 4: // MDR
                return "MDR";
            case 5: // A
                return "A";
            case 6: // Q
                return "Q";
            case 7: // X1
                return "X1";
            case 8: // X2
                return "X2";
            case 9: // X3
                return "X3";
            case 10: // I
                return "I";
            case 11: // S
                return "S";
            case 12: // ICTEMP
                return "ICTEMP";
            case 13: // EA
                return "EA";
            default:
                return "???";
        }
    }

    public static String getIn(int in) {
        switch (in) {
            case 0: // No signal
                return "NONE";
            case 1: // IC
                return "IC";
            case 2: // MAR
                return "MAR";
            case 3: // IDR
                return "IDR";
            case 4: // MDR
                return "MDR";
            case 5: // A
                return "A";
            case 6: // Q
                return "Q";
            case 7: // X1
                return "X1";
            case 8: // X2
                return "X2";
            case 9: // X3
                return "X3";
            case 10: // I
                return "I";
            case 11: // S
                return "S";
            case 12: // ICTEMP
                return "ICTEMP";
            case 13: // EA
                return "EA";
            default:
                return "???";
        }
    }

    public static String getIt(int it) {
        switch (it) {
            case 0:
                return "R TO R";
            case 1:
                return "READ";
            case 2:
                return "READ PCINCR";
            case 3:
                return "WRITE";
            case 4:
                return "GROUP BRANCH";
            case 5:
                return "INDIRECT BRANCH";
            case 6:
                return "ADD";
            case 7:
                return "OP CODE BRANCH";
            default:
                return "???";
        }
    }
}
