package com.gothasoftware.dn355.processor;

import java.util.LinkedList;
import java.util.List;

public class Memory {

    private static final int MEMORY_MASK = Constants.BIT_MASKS[18];

    private final int[] memoryStore = new int[Constants.MAX_MEMORY];

    private final List<MemoryChangeListener> listeners = new LinkedList<>();

    public void addChangeListener(MemoryChangeListener listener) {
        listeners.add(listener);
    }

    public int getMaxMemory() {
        return Constants.MAX_MEMORY;
    }

    public int read(int address) {
        return memoryStore[address] & MEMORY_MASK;
    }

    public void write(int address, int newValue) {
        int oldValue = memoryStore[address] & MEMORY_MASK;
        memoryStore[address] = newValue & MEMORY_MASK;
        listeners.forEach(listener -> listener.memoryChanged(address, oldValue, newValue));
    }
}
