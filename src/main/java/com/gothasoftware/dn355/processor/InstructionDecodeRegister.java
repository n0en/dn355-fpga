package com.gothasoftware.dn355.processor;

/*

    INSTRUCTION FORMAT (dd01, pg 304).
   
    Memory Reference Instructions
   
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    | I |   T   |    OPCODE             |      D                            |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
   
    I      Indirect bit: when on, the effective address is computed from the 
           indirect word.
    T      Tag Field: specifies address modification using an index register (X1,
           X2, X3, or the instruction counter (IC).
    Opcode Operation code.
    D      Displacement.
   

    The basic method of forming effective addresses consists of adding the
    9-but displacement field (D) to the complete address obtained from one of
    the three index registers of the instruction counter. The displacement (D) 
    is treated as a 9-bit number in 2's complement form. When indirect 
    addressing is used as the address of an indirect word with the following
    format:
   
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    | I |   T   |    Y                                                      |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
   
    This differs from the instruction word, in that Y is an address field
    rather than a displacement field, and no base address is needed to form a
    a full 15-bit address. The I specifies further indirect addressing. [CAC: 
    what about T?]
   
   
    Non-memory reference instructions
   
    Group 1
   
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |     S1    |    OPCODE             |      D                            |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
   
     S1   Sub-operation code
   
    Group 2
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |     S1    |    OPCODE             |     S2    |     K                 |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
   
    S1, S2   Sub-operation codes; These two code form a prefix and a suffix
             to the operation codes and are used to determine the instruction
             within the group.
    K        Operation value: This field is used for such functions as shift
             counts.

    ADDRESS FORMATION

                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |     CY    |       WY                                                  |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
   
     CY   character address
     WY   word address
   
       Fractional       CY
     Interpretation   Value    Character addressed
     --------------   -----    -------------------
        0/3            100     6-bit character number 0 (bits 0-5)
        1/3            101     6-bit character number 1 (bits 6-11)
        2/3            110     6-bit character number 2 (bits 12-17)
        0/2            010     9-bit character number 0 (bits 0-8)
        1/2            011     9-bit character number 1 (bits 9-17)
      Full word        000     Full word
   
    When characters are address, they are transfered to and from memory 
    right justified. Unused bit are zeros for operations from memory and are
    ignored for operations to memory.
   
    An example of load and store upon 6-bit character No. 0 (CY = 100):
   
    Memory to processor or IOM:
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |   CHARACTER           |  ignored...                                   |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    \                      /
     -----------v----------
                |
                +-----------------------------------------------+
                                                                |
                                                                ^
                                                     ----------------------
                                                    /                      \\
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |   Zeros                                       |   CHARACTER           |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
   
    Processor or IOM to memory:
   
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |   Ignored                                     |   CHARACTER           |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
                                                     \                      /
                                                      -----------v----------
                                                                 |
                +-----------------------------------------------+
                |
                ^
      ----------------------
     /                      \\
                                              1   1   1   1   1   1   1   1
      0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    |  CHARACTER            |   Unchanged                                   |
    +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+



    BASIC EFFECTIVE ADDRESS FORMATION
   
     -------------------------------------------------------------------
     |       | Instruction word            | Indirect word             |
     |       -----------------------------------------------------------
     |       |  I=0         |  I=1         |  I=9       |  I=1         |
     -------------------------------------------------------------------
     | T=00  |  Y**=IC+D    |  Y*=C(IC+D)  |  Y**=Y     |  Y*=C(Y)     |
     | T=01  |  Y**=X1+D    |  Y*=C(X1+D)  |  Y**=X1+Y  |  Y*=C(X1+Y)  |
     | T=02  |  Y**=X2+D    |  Y*=C(X2+D)  |  Y**=X2+Y  |  Y*=C(X2+Y)  |
     | T=03  |  Y**=X3+D    |  Y*=C(X3+D)  |  Y**=X3+Y  |  Y*=C(X3+Y)  |
     -------------------------------------------------------------------

 */

@SuppressWarnings("OctalInteger")
public class InstructionDecodeRegister extends SimpleRegister {

    // Operation code (for all formats)
    private static final int OP_CODE_MASK = 0077000;
    private static final int OP_CODE_SHIFT = 9;

    // Indirect memory access
    private static final int I_MASK = 0400000;

    // Address modification tag
    private static final int TAG_MASK = 0300000;
    private static final int TAG_SHIFT = 15;

    // Group 1 Op Codes
    private static final int S1_MASK = 0700000;
    private static final int S1_SHIFT = 15;

    // Group 2 Op Codes
    private static final int S2_MASK = 0000700;
    private static final int S2_SHIFT = 6;

    private static final int K_MASK = 0000077;

    // Displacement
    private static final int D_MASK = 0000777;
    private static final int D_SIGN_MASK = 0000400;
    private static final int D_EXTENSION = 0777000;

    private OpCodes opCode;
    private Group1OpCodes group1OpCode;
    private Group2OpCodes group2OpCode;

    private final Signal signalIndirect;
    private final Signal signalTag;

    public InstructionDecodeRegister(String name, Signal signalIn, Signal signalOut, Processor processor, Clock clock) {
        super(name, 18, signalIn, signalOut, processor, clock);

        signalIndirect = processor.getControlBus().getSignalIndirect();
        signalTag = processor.getControlBus().getSignalTAG_out();
    }

    @Override
    public void setValue(int value) {
        int oldValue = this.value & bitMask;
        this.value = value & bitMask;

        opCode = OpCodes.findOpCode(getOpCode());
        group1OpCode = null;

        signalIndirect.releaseSignal();
        signalTag.releaseSignal();

        switch (getGroup()) {
            case 0: // Memory Reference Instructions
                if (isIndirect()) {
                    signalIndirect.assertSignal();
                }
                break;

            case 1: // Group 1 Non-memory Reference Instructions
                group1OpCode = Group1OpCodes.findOpCode(getOpCode(), getS1());
                break;

            case 2: // Group 2 Non-memory Reference Instructions
                group2OpCode = Group2OpCodes.findOpCode(getOpCode(), getS1(), getS2());
                break;

            default:
                throw new RuntimeException("Invalid instruction group " + getGroup());
        }

        listeners.forEach(listener -> listener.registerValueChanged(oldValue, this.value));
    }

    public boolean isIndirect() {
        return (super.getValue() & I_MASK) != 0;
    }

    public int getOpCode() {
        return (super.getValue() & OP_CODE_MASK) >> OP_CODE_SHIFT;
    }

    public int getTag() {
        return (super.getValue() & TAG_MASK) >> TAG_SHIFT;
    }

    public int getDisplacement() {
        return super.getValue() & D_MASK;
    }

    public int getS1() {
        return (super.getValue() & S1_MASK) >> S1_SHIFT;
    }

    public int getS2() {
        return (super.getValue() & S2_MASK) >> S2_SHIFT;
    }

    public int getK() {
        return super.getValue() & K_MASK;
    }

    @Override
    public void clockRisingEdge() {
        Signal signalOut = getSignalOut();
        if ((signalOut != null) && signalOut.isSignalAsserted()) {
            // Displacement is 9 bits in 2's complement form so we need to do sign extension
            // when putting it in the data path
            int displacement = getDisplacement();
            if ((displacement & D_SIGN_MASK) != 0) {
                displacement |= D_EXTENSION;
            }
            getProcessor().setDataPath(displacement);
        }
    }

    public int getIndirect() {
        return isIndirect() ? 1 : 0;
    }

    public int getGroup() {
        return opCode.getGroup();
    }

    public Group1OpCodes getGroup1OpCode() {
        return group1OpCode;
    }

    public Group2OpCodes getGroup2OpCode() {
        return group2OpCode;
    }

}
