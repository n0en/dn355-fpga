package com.gothasoftware.dn355.processor;

@SuppressWarnings("OctalInteger")
public enum Group2OpCodes {

    CAX2(033, 00, 02),
    LLS(033, 00, 04),
    LRS(033, 00, 05),
    ALS(033, 00, 06),
    ARS(033, 00, 07),
    NRML(033, 01, 04),
    NRM(033, 01, 06),
    NOP(033, 02, 01),
    CX1A(033, 02, 02),
    LLR(033, 02, 04),
    LRL(033, 02, 05),
    ALR(033, 02, 06),
    ARL(033, 02, 07),
    INH(033, 03, 01),
    CX2A(033, 03, 02),
    CX3A(033, 03, 03),
    ALP(033, 03, 06),
    DIS(033, 04, 01),
    CAX1(033, 04, 02),
    CAX3(033, 04, 03),
    QLS(033, 04, 06),
    QRS(033, 04, 07),
    CAQ(033, 06, 03),
    QLR(033, 06, 06),
    QRL(033, 06, 07),
    ENI(033, 07, 01),
    CQA(033, 07, 03),
    QLP(033, 07, 06),
    ;

    private final int opCode;
    private final int s1;
    private final int s2;

    Group2OpCodes(int opCode, int s1, int s2) {
        this.opCode = opCode;
        this.s1 = s1;
        this.s2 = s2;
    }

    public int getOpCode() {
        return opCode;
    }

    public int getS1() {
        return s1;
    }

    public int getS2() {
        return s2;
    }

    public static Group2OpCodes findOpCode(int opCode, int s1, int s2) {
        for (Group2OpCodes code : values()) {
            if ((code.opCode == opCode) && (code.s1 == s1) && (code.s2 == s2)) {
                return code;
            }
        }
        throw new IllegalArgumentException("Failed to find operation code " + opCode + " (s1=" + s1 + ", s2=" + s2 + ")");
    }

}
