package com.gothasoftware.dn355.processor;

public class IncrementableRegister extends AbstractRegister {

    private final Signal signalIncrement;

    IncrementableRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock, Signal signalIncrement) {
        super(name, bits, signalIn, signalOut, processor, clock, null, null);

        this.signalIncrement = signalIncrement;
    }

    private void increment() {
        setValue(getValue() + 1);
    }

    @Override
    public void clockRisingEdge() {
        super.clockRisingEdge();

        System.out.println("INCR: Signal " + signalIncrement.getName() + " is " + signalIncrement.isSignalAsserted());
        if (signalIncrement.isSignalAsserted()) {
            increment();
        }
    }

}
