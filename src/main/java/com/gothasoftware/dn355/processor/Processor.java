package com.gothasoftware.dn355.processor;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("OctalInteger")
public class Processor {

    private final Clock clock;

    private final Memory memory;

    private final ControlUnit controlUnit;

    private final ControlBus controlBus;

    private final ArithmeticLogicUnit alu;

    // Macro Instruction Registers
    private final SimpleRegister registerA;
    private final SimpleRegister registerQ;
    private final SimpleRegister registerX1;
    private final SimpleRegister registerX2;
    private final SimpleRegister registerX3;
    private final IndicatorRegister registerI;
    private final SimpleRegister registerS;

    private final IncrementableRegister registerIC;
    private final SimpleRegister registerICTEMP;

    private final SimpleRegister registerEA;

    private final SimpleRegister dataPath; // The current value placed on the data path

    private final List<ResetableComponent> resetableComponents = new ArrayList<>();


    public Processor() {
        controlBus = new ControlBus();
        resetableComponents.add(controlBus);

        clock = new Clock();
        resetableComponents.add(clock);


        memory = new Memory();

        controlUnit = new ControlUnit(this);
        resetableComponents.add(controlUnit);

        registerA = new AddableRegister("A", 18, controlBus.getSignalA_in(), controlBus.getSignalA_out(),
                this, clock, controlBus.getSignalAdd(), controlBus.getSignalZ_en());
        resetableComponents.add(registerA);

        registerQ = new AddableRegister("Q", 18, controlBus.getSignalQ_in(), controlBus.getSignalQ_out(),
                this, clock, controlBus.getSignalAdd(), controlBus.getSignalZ_en());
        resetableComponents.add(registerQ);

        registerX1 = new SimpleRegister("X1", 18, controlBus.getSignalX1_in(), controlBus.getSignalX1_out(),
                this, clock, controlBus.getSignalZ_en());
        resetableComponents.add(registerX1);

        registerX2 = new SimpleRegister("X2", 18, controlBus.getSignalX2_in(), controlBus.getSignalX2_out(),
                this, clock, controlBus.getSignalZ_en());
        resetableComponents.add(registerX2);

        registerX3 = new SimpleRegister("X3", 18, controlBus.getSignalX3_in(), controlBus.getSignalX3_out(),
                this, clock, controlBus.getSignalZ_en());
        resetableComponents.add(registerX3);

        registerI = new IndicatorRegister("I", 8, controlBus.getSignalI_in(), controlBus.getSignalI_out(), this, clock);
        resetableComponents.add(registerI);

        registerS = new SimpleRegister("S", 6, controlBus.getSignalS_in(), controlBus.getSignalS_out(), this, clock);
        resetableComponents.add(registerS);

        registerIC = new IncrementableRegister("IC", 15, controlBus.getSignalIC_in(),
                controlBus.getSignalIC_out(), this, clock, controlBus.getSignalIC_incr());
        resetableComponents.add(registerIC);

        registerICTEMP = new SimpleRegister("ICTEMP", 15, controlBus.getSignalICTEMP_in(),
                controlBus.getSignalICTEMP_out(), this, clock);
        resetableComponents.add(registerICTEMP);

        registerEA = new AddableRegister("EA", 15, controlBus.getSignalEA_in(), controlBus.getSignalEA_out(),
                this, clock, controlBus.getSignalAdd(), null);
        resetableComponents.add(registerEA);

        dataPath = new SimpleRegister("Data Path", 18, null, null, this, clock);
        resetableComponents.add(dataPath);

        alu = new ArithmeticLogicUnit(this);

        // Load an initial simple program to test the processor
        memory.write(0, 0673144);   // ILA  =100
        memory.write(1, 0773140);   // IAA  =96
        memory.write(2, 0633300);   // CAQ
        memory.write(3, 0573007);   // IAQ  =7
        memory.write(4, 0071001);   // TRA 5
        memory.write(5, 0673000);   // ILA  =0
        memory.write(6, 0433200);   // CAX1
        memory.write(7, 0071771);   // TRA 0
    }

    public void setDataPath(int dataPath) {
        this.dataPath.setValue(dataPath);
    }

    public SimpleRegister getRegisterDataPath() {
        return dataPath;
    }

    public Clock getClock() {
        return clock;
    }

    public ControlUnit getControlUnit() {
        return controlUnit;
    }

    public Memory getMemory() {
        return memory;
    }

    public ControlBus getControlBus() {
        return controlBus;
    }

    public SimpleRegister getRegisterA() {
        return registerA;
    }

    public SimpleRegister getRegisterQ() {
        return registerQ;
    }

    public SimpleRegister getRegisterX1() {
        return registerX1;
    }

    public SimpleRegister getRegisterX2() {
        return registerX2;
    }

    public SimpleRegister getRegisterX3() {
        return registerX3;
    }

    public IndicatorRegister getRegisterI() {
        return registerI;
    }

    public SimpleRegister getRegisterS() {
        return registerS;
    }

    public IncrementableRegister getRegisterIC() {
        return registerIC;
    }

    public SimpleRegister getRegisterICTEMP() {
        return registerICTEMP;
    }

    public SimpleRegister getRegisterEA() {
        return registerEA;
    }

    /**
     * Perform a processor reset
     */
    public void reset() {
        resetableComponents.forEach(ResetableComponent::reset);
    }

    /**
     * Step one micro-instruction
     */
    public void step() {
        clock.pulse();
    }

}
