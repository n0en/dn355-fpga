package com.gothasoftware.dn355.processor;

import java.util.LinkedList;
import java.util.List;

public class Clock implements ResetableComponent {

    enum ClockState {PrePulseInit, RisingEdge, FallingEdge}

    private int pulseCount;
    private ClockState currentState = ClockState.FallingEdge;

    private final List<ClockListener> listeners = new LinkedList<>();

    public void addListener(ClockListener listener) {
        listeners.add(listener);
    }

    ClockState step() {
        switch (currentState) {
            case PrePulseInit:
                currentState = ClockState.RisingEdge;
                listeners.forEach(ClockListener::clockRisingEdge);
                break;
            case RisingEdge:
                currentState = ClockState.FallingEdge;
                listeners.forEach(ClockListener::clockFallingEdge);
                pulseCount++;
                break;
            case FallingEdge:
                currentState = ClockState.PrePulseInit;
                listeners.forEach(ClockListener::clockPrePulseInit);
                break;
        }

        return currentState;
    }

    void pulse() {
        //noinspection StatementWithEmptyBody
        while (step() != ClockState.FallingEdge) {
            // All action is invoked by calling step() above
        }
    }

    public void reset() {
        pulseCount = 0;
        currentState = ClockState.FallingEdge;
    }

    public int getPulseCount() {
        return pulseCount;
    }
}
