package com.gothasoftware.dn355.processor;

/*
    Indicator register (as defined by the LDI/STI instructions (DD01, pg 3-12)

      bit 
    position    indicator
   
       0        Zero
       1        Negative
       2        Carry
       3        Overflow
       4        Interrupt inhibit
       5        Parity fault inhibit
       6        Overflow fault inhibit
       7        Parity error
*/

@SuppressWarnings("OctalInteger")
public class IndicatorRegister extends SimpleRegister {

    private static final int MASK_ZERO = 0001;
    private static final int MASK_NEGATIVE = 0002;
    private static final int MASK_CARRY = 0004;
    private static final int MASK_OVERFLOW = 0010;
    private static final int MASK_INTERRUPT_INHIBIT = 0020;
    private static final int MASK_PARITY_FAULT_INHIBIT = 0040;
    private static final int MASK_OVERFLOW_FAULT_INHIBIT = 0100;
    private static final int MASK_PARITY_ERROR = 0200;

    private final Indicator zeroIndicator;
    private final Indicator negativeIndicator;
    private final Indicator carryIndicator;
    private final Indicator overflowIndicator;
    private final Indicator interruptInhibitIndicator;
    private final Indicator parityFaultInhibitIndicator;
    private final Indicator overflowFaultInhibitIndicator;
    private final Indicator parityErrorIndicator;

    public IndicatorRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock) {
        super(name, bits, signalIn, signalOut, processor, clock);

        zeroIndicator = new Indicator("Z", MASK_ZERO, this);
        negativeIndicator = new Indicator("N", MASK_NEGATIVE, this);
        carryIndicator = new Indicator("C", MASK_CARRY, this);
        overflowIndicator = new Indicator("O", MASK_OVERFLOW, this);
        interruptInhibitIndicator = new Indicator("II", MASK_INTERRUPT_INHIBIT, this);
        parityFaultInhibitIndicator = new Indicator("PFI", MASK_PARITY_FAULT_INHIBIT, this);
        overflowFaultInhibitIndicator = new Indicator("OFI", MASK_OVERFLOW_FAULT_INHIBIT, this);
        parityErrorIndicator = new Indicator("PE", MASK_PARITY_ERROR, this);
    }

    public Indicator getZeroIndicator() {
        return zeroIndicator;
    }

    public void setZeroIndicator(boolean value) {
        zeroIndicator.setActive(value);
    }

    public Indicator getNegativeIndicator() {
        return negativeIndicator;
    }

    public void setNegativeIndicator(boolean value) {
        negativeIndicator.setActive(value);
    }

    public Indicator getCarryIndicator() {
        return carryIndicator;
    }

    public Indicator getOverflowIndicator() {
        return overflowIndicator;
    }

    public Indicator getInterruptInhibitIndicator() {
        return interruptInhibitIndicator;
    }

    public Indicator getParityFaultInhibitIndicator() {
        return parityFaultInhibitIndicator;
    }

    public Indicator getOverflowFaultInhibitIndicator() {
        return overflowFaultInhibitIndicator;
    }

    public Indicator getParityErrorIndicator() {
        return parityErrorIndicator;
    }

}
