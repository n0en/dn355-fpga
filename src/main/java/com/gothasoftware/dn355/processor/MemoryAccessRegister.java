package com.gothasoftware.dn355.processor;

public class MemoryAccessRegister extends SimpleRegister {

    private final SimpleRegister memoryAddressRegister;
    private final Signal signalRead;
    private final Signal signalWrite;

    private final Memory memory;

    public MemoryAccessRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock,
                                SimpleRegister memoryAddressRegister, Signal signalRead, Signal signalWrite, Memory memory) {
        super(name, bits, signalIn, signalOut, processor, clock);

        this.memoryAddressRegister = memoryAddressRegister;
        this.signalRead = signalRead;
        this.signalWrite = signalWrite;

        this.memory = memory;
    }

    @Override
    public void clockRisingEdge() {
        super.clockRisingEdge();

        if (signalRead.isSignalAsserted()) {
            setValue(memory.read(memoryAddressRegister.getValue()));
        } else if (signalWrite.isSignalAsserted()) {
            memory.write((int)memoryAddressRegister.getValue(), getValue());
        }
    }
}
