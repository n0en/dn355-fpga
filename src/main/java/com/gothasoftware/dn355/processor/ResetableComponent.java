package com.gothasoftware.dn355.processor;

public interface ResetableComponent {
    void reset();
}
