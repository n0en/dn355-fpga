package com.gothasoftware.dn355.processor;

public interface RegisterListener {

    void registerValueChanged(long oldValue, long newValue);

}
