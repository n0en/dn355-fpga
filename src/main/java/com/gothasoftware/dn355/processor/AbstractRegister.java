package com.gothasoftware.dn355.processor;

import java.util.LinkedList;
import java.util.List;

abstract class AbstractRegister implements ClockListener, Register, ResetableComponent {

    private final String name;
    protected final Signal signalIn;
    private final Signal signalOut;
    private final Processor processor;
    protected final int bitMask;
    private final int negativeBitMask;
    private final int bits;
    private final Signal signalZenable;
    private final Signal signalNenable;

    protected int value;

    protected List<RegisterListener> listeners = new LinkedList<>();

    AbstractRegister(String name, int bits, Signal signalIn, Signal signalOut, Processor processor, Clock clock, Signal signalZenable, Signal signalNenable) {
        this.name = name;
        this.signalIn = signalIn;
        this.signalOut = signalOut;
        this.signalZenable = signalZenable;
        this.signalNenable = signalNenable;
        this.processor = processor;
        this.bitMask = Constants.BIT_MASKS[bits];
        this.bits = bits;
        this.negativeBitMask = 1 << (bits - 1);
        clock.addListener(this);
    }

    public void addListener(RegisterListener listener) {
        listeners.add(listener);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void clockRisingEdge() {
        if ((signalOut != null) && signalOut.isSignalAsserted()) {
            processor.setDataPath(getValue());
        }
    }

    @Override
    public void clockFallingEdge() {
        if ((signalIn != null) && signalIn.isSignalAsserted()) {
            setValue(processor.getRegisterDataPath().getValue());
        }
    }

    @Override
    public void clockPrePulseInit() {
        // Don't care for now
    }

    public void reset() {
        setValue(0);
    }

    @Override
    public int getSizeBits() {
        return bits;
    }

    public int getValue() {
        return value & bitMask;
    }

    public void setValue(int value) {
        int oldValue = this.value & bitMask;
        this.value = value & bitMask;
        if ((signalZenable != null) && signalZenable.isSignalAsserted()) {
            processor.getRegisterI().setZeroIndicator(this.value == 0);
        }
        if ((signalNenable != null) && signalNenable.isSignalAsserted()) {
            processor.getRegisterI().setNegativeIndicator((this.value & negativeBitMask) != 0);
        }
        listeners.forEach(listener -> listener.registerValueChanged(oldValue, this.value));
    }

    Signal getSignalOut() {
        return signalOut;
    }

    Processor getProcessor() {
        return processor;
    }

}
