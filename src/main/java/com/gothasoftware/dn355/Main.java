package com.gothasoftware.dn355;


import com.gothasoftware.dn355.gui.GuiMain;
import com.gothasoftware.dn355.processor.Processor;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        try {
            new GuiMain(new Processor());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Exception from main", e);
        }
    }
}
