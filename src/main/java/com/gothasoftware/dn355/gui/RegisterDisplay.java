package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.Clock;
import com.gothasoftware.dn355.processor.ClockListener;
import com.gothasoftware.dn355.processor.Register;
import com.gothasoftware.dn355.processor.RegisterListener;

import javax.swing.*;
import java.awt.*;

public class RegisterDisplay extends JTextField implements ClockListener, RegisterListener {

    private final int displayDigits;

    public RegisterDisplay(String defaultValue, Register register, Clock clock) {
        super(defaultValue);

        register.addListener(this);

        clock.addListener(this);

        if ((register.getSizeBits() % 3) != 0) {
            displayDigits = (register.getSizeBits() / 3) + 1;
        } else {
            displayDigits = register.getSizeBits() / 3;
        }
    }

    @Override
    public void registerValueChanged(long oldValue, long newValue) {
        setText(MemoryDisplayFrame.padLeft(Long.toOctalString(newValue), "0", displayDigits));
        setToolTipText(MemoryDisplayFrame.padLeft(Long.toOctalString(oldValue), "0", displayDigits));
        setBackground(Color.YELLOW);
    }

    @Override
    public void clockPrePulseInit() {
        setBackground(Color.WHITE);
    }

    @Override
    public void clockRisingEdge() {
        // Don't care
    }

    @Override
    public void clockFallingEdge() {
        // Don't care
    }
}
