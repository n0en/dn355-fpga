package com.gothasoftware.dn355.gui;


import com.gothasoftware.dn355.processor.*;

import javax.swing.*;
import java.awt.*;

public class InstructionDecodeFrame extends AbstractDisplayFrame implements RegisterListener {

    private static final Dimension MNEMONIC_BUTTON_SIZE = new Dimension(110, 20);

    private final transient SimpleRegister registerI;
    private final transient SimpleRegister registerT;
    private final transient SimpleRegister registerOp;
    private final transient SimpleRegister registerD;
    private final transient SimpleRegister registerS1;

    private final transient Processor processor;

    private final transient InstructionDecodeRegister registerIDR;

    private JButton mnemonicButton;


    public InstructionDecodeFrame(Processor processor) {
        super("Instruction Decode", "instruction.decode", 50, 50);

        this.processor = processor;

        registerIDR = processor.getControlUnit().getRegisterIDR();

        registerI = new SimpleRegister("Indirect", 1, null, null, processor, processor.getClock());
        registerT = new SimpleRegister("Tag", 3, null, null, processor, processor.getClock());
        registerOp = new SimpleRegister("Opcode", 6, null, null, processor, processor.getClock());
        registerD = new SimpleRegister("Displacement", 9, null, null, processor, processor.getClock());
        registerS1 = new SimpleRegister("S1", 3, null, null, processor, processor.getClock());

        initComponentsLocal();

        registerIDR.addListener(this);
    }

    private void initComponentsLocal() {
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        int row = 0;

        addRegister("000000", row, c, processor.getControlUnit().getRegisterIDR(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalIDR_in());

        row++;

        addRegister("0", row, c, registerI, processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalIndirect());

        row++;

        addRegister("0", row, c, registerT, processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalTAG_out());

        row++;

        addRegister("0", row, c, registerS1, processor.getClock());

        row++;

        addRegister("0", row, c, registerOp, processor.getClock());

        mnemonicButton = new JButton("");
        mnemonicButton.setPreferredSize(MNEMONIC_BUTTON_SIZE);
        mnemonicButton.setMinimumSize(MNEMONIC_BUTTON_SIZE);
        c.gridx = 2;
        c.gridy = row;
        mnemonicButton.setBackground(Color.LIGHT_GRAY);
        add(mnemonicButton, c);

        row++;

        addRegister("0", row, c, registerD, processor.getClock());

        pack();
    }

    @Override
    public void registerValueChanged(long oldValue, long newValue) {
        registerI.setValue(registerIDR.getIndirect());
        registerT.setValue(registerIDR.getTag());
        registerOp.setValue(registerIDR.getOpCode());
        registerD.setValue(registerIDR.getDisplacement());
        registerS1.setValue(registerIDR.getS1());

        switch (registerIDR.getGroup()) {
            case 0:
                mnemonicButton.setText(OpCodes.findOpCode(registerIDR.getOpCode()).toString());
                break;
            case 1:
                mnemonicButton.setText(registerIDR.getGroup1OpCode().toString());
                break;
            case 2:
                mnemonicButton.setText(registerIDR.getGroup2OpCode().toString());
                break;
            default:
                mnemonicButton.setText("???");
                break;
        }
    }
}
