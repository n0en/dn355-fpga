package com.gothasoftware.dn355.gui;


import com.gothasoftware.dn355.processor.Processor;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GuiMain extends javax.swing.JFrame {
    private static final Logger LOGGER = Logger.getLogger(GuiMain.class.getName());

    private static final String PROPERTY_FILE_NAME = "dn355.properties";

    private static final String PROP_MAIN_X = "main.x";
    private static final String PROP_MAIN_Y = "main.y";
    private static final String PROP_MAIN_HEIGHT = "main.height";
    private static final String PROP_MAIN_WIDTH = "main.width";

    private static Properties properties = null;

    static {
        try {
            properties = new Properties();
            properties.load(new FileReader(PROPERTY_FILE_NAME));
        } catch (FileNotFoundException e) {
            // Don't care, we will just have empty properties
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Exception loading properties", e);
        }
    }

    private final transient Processor processor;

    public GuiMain(Processor processor) {
        this.processor = processor;

        initComponents();

        int x = getIntProperty(PROP_MAIN_X, 40);
        int y = getIntProperty(PROP_MAIN_Y, 40);
        int h = getIntProperty(PROP_MAIN_HEIGHT, 600);
        int w = getIntProperty(PROP_MAIN_WIDTH, 500);

        setSize(w, h);
        setLocation(x, y);
    }

    private void initComponents() {
        JDesktopPane desktopPane = new JDesktopPane();
        javax.swing.JMenuBar menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();

        setTitle("Microcode Simulator");
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm();
            }
        });

        getContentPane().add(desktopPane, BorderLayout.CENTER);

        fileMenu.setText("File");

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(evt -> exitMenuItemActionPerformed());

        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        ProcessorControlFrame processorControlFrame = new ProcessorControlFrame(processor);
        desktopPane.add(processorControlFrame);
        processorControlFrame.show();

        ControlUnitDisplayFrame controlUnitDisplayFrame = new ControlUnitDisplayFrame(processor);
        desktopPane.add(controlUnitDisplayFrame);
        controlUnitDisplayFrame.show();

        ProcessorInternalDisplayFrame processorInternalDisplayFrame = new ProcessorInternalDisplayFrame(processor);
        desktopPane.add(processorInternalDisplayFrame);
        processorInternalDisplayFrame.show();

        MemoryDisplayFrame memoryDisplayFrame = new MemoryDisplayFrame(processor.getMemory(), processor.getClock());
        desktopPane.add(memoryDisplayFrame);
        memoryDisplayFrame.show();

        InstructionDecodeFrame instructionDecodeFrame = new InstructionDecodeFrame(processor);
        desktopPane.add(instructionDecodeFrame);
        instructionDecodeFrame.show();

        InstructionTraceFrame instructionTraceFrame = new InstructionTraceFrame(processor);
        desktopPane.add(instructionTraceFrame);
        instructionTraceFrame.show();

        MicrocodeTraceFrame microcodeTraceFrame = new MicrocodeTraceFrame(processor);
        desktopPane.add(microcodeTraceFrame);
        microcodeTraceFrame.show();

        setVisible(true);
    }

    private void exitMenuItemActionPerformed() {
        Dimension dimension = getSize();

        setIntProperty(PROP_MAIN_HEIGHT, dimension.height);
        setIntProperty(PROP_MAIN_WIDTH, dimension.width);

        setIntProperty(PROP_MAIN_X, getX());
        setIntProperty(PROP_MAIN_Y, getY());

        try {
            properties.store(new FileWriter(PROPERTY_FILE_NAME), "Microcode Simulator Properties");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Exception storing properties", e);
        }

        System.exit(0);
    }

    /**
     * Exit the Application
     */
    private void exitForm() {
        exitMenuItemActionPerformed();
        System.exit(0);
    }

    static int getIntProperty(String name, int defaultValue) {
        String value = properties.getProperty(name, String.valueOf(defaultValue));
        return Integer.parseInt(value);
    }

    static void setIntProperty(String name, int value) {
        properties.setProperty(name, String.valueOf(value));
    }

}
