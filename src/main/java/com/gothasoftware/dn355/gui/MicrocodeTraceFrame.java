package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.*;

import javax.swing.*;
import java.awt.*;
import java.util.StringJoiner;
import java.util.TreeSet;

public class MicrocodeTraceFrame extends AbstractDisplayFrame implements ClockListener, SignalListener {

    private final DefaultListModel<String> model = new DefaultListModel<>();

    private final TreeSet<String> assertedSignals = new TreeSet<>();
    private final TreeSet<String> releasedSignals = new TreeSet<>();

    private final transient Clock clock;
    private final transient ControlUnit controlUnit;


    public MicrocodeTraceFrame(Processor processor) {
        super("Microcode Trace", "microcode.trace", 70, 70);

        this.clock = processor.getClock();
        this.controlUnit = processor.getControlUnit();

        initComponentsLocal();

        processor.getControlBus().addAllSignalListener(this);
        clock.addListener(this);
    }

    private void initComponentsLocal() {

        JScrollPane scrollPane = new JScrollPane();
        JList<String> microcodeList = new JList<>();

        microcodeList.setModel(model);
        microcodeList.setBackground(Color.getColor("#DDDDDD"));
        microcodeList.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));

        scrollPane.setViewportView(microcodeList);
        add(scrollPane, BorderLayout.CENTER);

        pack();

    }

    @Override
    public void clockPrePulseInit() {
//        addTrace("P");
    }

    @Override
    public void clockRisingEdge() {
//        addTrace("R");
    }

    @Override
    public void clockFallingEdge() {
        addTrace("F");
    }

    @Override
    public void signalAsserted(String name) {
        assertedSignals.add(name);
        releasedSignals.remove(name);
    }

    @Override
    public void signalReleased(String name) {
        assertedSignals.remove(name);
        releasedSignals.add(name);
    }

    private void addTrace(String clockPhase) {
        StringJoiner assertedString = new StringJoiner("|");
        assertedSignals.forEach(assertedString::add);

        model.addElement(String.format("%5d: [%s] CSAR=%04o OUT=%-7s IN=%-7s IT=%-18s FLAGS=%02o [A: %s]",
                clock.getPulseCount(),
                clockPhase,
                controlUnit.getRegisterCSAR().getValue(),
                ControlUnit.getOut(controlUnit.getCurrentOut()),
                ControlUnit.getIn(controlUnit.getCurrentIn()),
                ControlUnit.getIt(controlUnit.getCurrentIt()),
                controlUnit.getCurrentFlags(),
                assertedString));
    }
}
