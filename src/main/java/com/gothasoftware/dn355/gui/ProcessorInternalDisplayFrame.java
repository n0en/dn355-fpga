package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.Processor;

import java.awt.*;

public class ProcessorInternalDisplayFrame extends AbstractDisplayFrame {

    private final transient Processor processor;

    ProcessorInternalDisplayFrame(Processor processor) {
        super("Processor Internals", "internals", 40, 40);
        this.processor = processor;
        initComponentsLocal();
    }


    private void initComponentsLocal() {
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        int row = 0;

        addRegister("000000", row, c, processor.getRegisterA(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalA_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalA_out());
        row++;

        addRegister("000000", row, c, processor.getRegisterQ(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalQ_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalQ_out());
        row++;

        addRegister("00000", row, c, processor.getRegisterX1(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalX1_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalX1_out());
        row++;

        addRegister("00000", row, c, processor.getRegisterX2(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalX2_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalX2_out());
        row++;

        addRegister("00000", row, c, processor.getRegisterX3(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalX3_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalX3_out());
        row++;

        addRegister("000", row, c, processor.getRegisterI(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalI_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalI_out());
        row++;

        addIndicator(row, 2, c, processor.getRegisterI().getZeroIndicator());
        addIndicator(row, 3, c, processor.getRegisterI().getNegativeIndicator());
        addIndicator(row, 4, c, processor.getRegisterI().getCarryIndicator());
        addIndicator(row, 5, c, processor.getRegisterI().getOverflowIndicator());
        row++;

        addIndicator(row, 2, c, processor.getRegisterI().getInterruptInhibitIndicator());
        addIndicator(row, 3, c, processor.getRegisterI().getParityFaultInhibitIndicator());
        addIndicator(row, 4, c, processor.getRegisterI().getOverflowFaultInhibitIndicator());
        addIndicator(row, 5, c, processor.getRegisterI().getParityErrorIndicator());
        row++;

        addRegister("00", row, c, processor.getRegisterS(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalS_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalS_out());
        row++;

        addRegister("00000", row, c, processor.getRegisterIC(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalIC_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalIC_out());
        addSignal(row, 4, c, processor.getControlBus().getSignalIC_incr());
        row++;

        addRegister("00000", row, c, processor.getRegisterICTEMP(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalICTEMP_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalICTEMP_out());
        row++;

        addRegister("00000", row, c, processor.getRegisterEA(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalEA_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalEA_out());
        row++;

        addRegister("00000", row, c, processor.getControlUnit().getRegisterMAR(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalMAR_in());
        row++;

        addRegister("000000", row, c, processor.getControlUnit().getRegisterMDR(), processor.getClock());
        addSignal(row, 2, c, processor.getControlBus().getSignalMDR_in());
        addSignal(row, 3, c, processor.getControlBus().getSignalMDR_out());
        addSignal(row, 4, c, processor.getControlBus().getSignalRead());
        addSignal(row, 5, c, processor.getControlBus().getSignalWrite());
        row++;

        addRegister("000000", row, c, processor.getRegisterDataPath(), processor.getClock());

        pack();
    }

}
