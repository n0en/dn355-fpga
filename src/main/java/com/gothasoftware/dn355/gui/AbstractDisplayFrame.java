package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.Clock;
import com.gothasoftware.dn355.processor.Indicator;
import com.gothasoftware.dn355.processor.Register;
import com.gothasoftware.dn355.processor.Signal;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractDisplayFrame extends JInternalFrame {

    private static final String PROP_X = ".x";
    private static final String PROP_Y = ".y";
    private static final String PROP_HEIGHT = ".height";
    private static final String PROP_WIDTH = ".width";

    private static final Dimension SIGNAL_DIM = new Dimension(120, 20);

    private static final Dimension REGISTER_LABEL_DIM = new Dimension(140, 19);
    private static final Dimension REGISTER_VALUE_DIM = new Dimension(80, 20);


    private final String propertyPrefix;

    AbstractDisplayFrame(String title, String propertyPrefix, int defaultX, int defaultY) {
        super(title, true, false, true, true);

        this.propertyPrefix = propertyPrefix;

        int x = GuiMain.getIntProperty(propertyPrefix + PROP_X, defaultX);
        int y = GuiMain.getIntProperty(propertyPrefix + PROP_Y, defaultY);
        int h = GuiMain.getIntProperty(propertyPrefix + PROP_HEIGHT, 400);
        int w = GuiMain.getIntProperty(propertyPrefix + PROP_WIDTH, 300);

        Dimension d = new Dimension(w, h);
        setSize(d);
        setPreferredSize(d);
        setLocation(x, y);

        initComponents();
    }

    private void initComponents() {
        addComponentListener(new java.awt.event.ComponentAdapter() {
            @Override
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                formComponentMoved();
            }

            @Override
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized();
            }
        });

    }

    private void formComponentResized() {
        Dimension d = getSize();

        GuiMain.setIntProperty(propertyPrefix + PROP_HEIGHT, d.height);
        GuiMain.setIntProperty(propertyPrefix + PROP_WIDTH, d.width);

        setPreferredSize(d);
    }

    private void formComponentMoved() {
        GuiMain.setIntProperty(propertyPrefix + PROP_X, getX());
        GuiMain.setIntProperty(propertyPrefix + PROP_Y, getY());
    }

    void addRegister(String defaultValue, int row, GridBagConstraints constraints, Register register, Clock clock) {
        JLabel label = new JLabel(register.getName() + ": ", SwingConstants.RIGHT);
        label.setPreferredSize(REGISTER_LABEL_DIM);
        label.setMinimumSize(REGISTER_LABEL_DIM);
        label.setVerticalAlignment(SwingConstants.CENTER);
        constraints.gridx = 0;
        constraints.gridy = row;
        add(label, constraints);

        JTextField textField = new RegisterDisplay(defaultValue, register, clock);
        textField.setPreferredSize(REGISTER_VALUE_DIM);
        textField.setMinimumSize(REGISTER_VALUE_DIM);
        constraints.gridx = 1;
        constraints.gridy = row;
        add(textField, constraints);

        label.setLabelFor(textField);
    }

    void addSignal(int row, int col, GridBagConstraints constraints, Signal signal) {
        JButton button = new SignalDisplay(signal);
        button.setPreferredSize(SIGNAL_DIM);
        button.setMinimumSize(SIGNAL_DIM);
        constraints.gridx = col;
        constraints.gridy = row;
        button.setBackground(Color.LIGHT_GRAY);
        add(button, constraints);
    }

    void addIndicator(int row, int col, GridBagConstraints constraints, Indicator indicator) {
        JButton button = new IndicatorDisplay(indicator);
        button.setPreferredSize(SIGNAL_DIM);
        button.setMinimumSize(SIGNAL_DIM);
        constraints.gridx = col;
        constraints.gridy = row;
        button.setBackground(Color.LIGHT_GRAY);
        add(button, constraints);
    }

}
