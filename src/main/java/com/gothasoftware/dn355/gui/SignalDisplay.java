package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.Signal;
import com.gothasoftware.dn355.processor.SignalListener;

import javax.swing.*;
import java.awt.*;

public class SignalDisplay extends JButton implements SignalListener {

    public SignalDisplay(Signal signal) {
        super(signal.getName());

        signal.addListener(this);
    }

    @Override
    public void signalAsserted(String name) {
        setBackground(Color.GREEN);
    }

    @Override
    public void signalReleased(String name) {
        setBackground(Color.LIGHT_GRAY);
    }
}
