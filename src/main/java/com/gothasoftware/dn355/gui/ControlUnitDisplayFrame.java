package com.gothasoftware.dn355.gui;


import com.gothasoftware.dn355.processor.ControlUnit;
import com.gothasoftware.dn355.processor.ControlUnitListener;
import com.gothasoftware.dn355.processor.Processor;
import com.gothasoftware.dn355.processor.SimpleRegister;

import javax.swing.*;
import java.awt.*;

public class ControlUnitDisplayFrame extends AbstractDisplayFrame implements ControlUnitListener {

    private static final Dimension LABEL_DIM = new Dimension(150, 19);

    private final transient Processor processor;
    private final transient ControlUnit controlUnit;

    private final transient SimpleRegister registerMicroinstruction;
    private final transient SimpleRegister registerFlags;
    private final transient SimpleRegister registerOut;
    private final transient SimpleRegister registerIn;
    private final transient SimpleRegister registerIt;
    private final transient SimpleRegister registerNext;

    private JLabel outLabel;
    private JLabel inLabel;
    private JLabel itLabel;

    public ControlUnitDisplayFrame(Processor processor) {
        super("Control Unit", "control_unit", 20, 20);

        this.processor = processor;
        this.controlUnit = processor.getControlUnit();

        registerMicroinstruction = new SimpleRegister("Micro-Instruction", 27, null, null, processor, processor.getClock());
        registerOut = new SimpleRegister("OUT", 6, null, null, processor, processor.getClock());
        registerIn = new SimpleRegister("IN", 6, null, null, processor, processor.getClock());
        registerIt = new SimpleRegister("IT", 6, null, null, processor, processor.getClock());
        registerFlags = new SimpleRegister("FLAGS", 6, null, null, processor, processor.getClock());
        registerNext = new SimpleRegister("NEXT", 9, null, null, processor, processor.getClock());

        initComponentsLocal();

        controlUnit.addListener(this);
    }

    private void initComponentsLocal() {
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        int row = 0;

        addRegister("000", row, c, controlUnit.getRegisterCSAR(), processor.getClock());
        row++;

        addRegister("000000000", row, c, registerMicroinstruction, processor.getClock());
        row++;

        addRegister("00", row, c, registerOut, processor.getClock());

        outLabel = new JLabel();
        outLabel.setPreferredSize(LABEL_DIM);
        outLabel.setMinimumSize(LABEL_DIM);
        outLabel.setVerticalAlignment(SwingConstants.CENTER);
        c.gridx = 2;
        c.gridy = row;
        add(outLabel, c);

        row++;

        addRegister("00", row, c, registerIn, processor.getClock());

        inLabel = new JLabel();
        inLabel.setPreferredSize(LABEL_DIM);
        inLabel.setMinimumSize(LABEL_DIM);
        inLabel.setVerticalAlignment(SwingConstants.CENTER);
        c.gridx = 2;
        c.gridy = row;
        add(inLabel, c);

        row++;

        addRegister("00", row, c, registerIt, processor.getClock());

        itLabel = new JLabel();
        itLabel.setPreferredSize(LABEL_DIM);
        itLabel.setMinimumSize(LABEL_DIM);
        itLabel.setVerticalAlignment(SwingConstants.CENTER);
        c.gridx = 2;
        c.gridy = row;
        add(itLabel, c);

        row++;

        addRegister("00", row, c, registerFlags, processor.getClock());

        row++;

        addRegister("000", row, c, registerNext, processor.getClock());

        row++;

        addSignal(row,0, c, processor.getControlBus().getSignalAdd());
        addSignal(row,1, c, processor.getControlBus().getSignalZ_en());
        addSignal(row,2, c, processor.getControlBus().getSignalN_en());

        pack();
    }

    @Override
    public void controlUnitMicroinstructionDecoded() {
        registerMicroinstruction.setValue(controlUnit.getCurrentMicroinstruction());
        registerFlags.setValue(controlUnit.getCurrentFlags());
        registerOut.setValue(controlUnit.getCurrentOut());
        registerIn.setValue(controlUnit.getCurrentIn());
        registerIt.setValue(controlUnit.getCurrentIt());
        registerNext.setValue(controlUnit.getCurrentNext());

        outLabel.setText(ControlUnit.getOut(controlUnit.getCurrentOut()));
        inLabel.setText(ControlUnit.getIn(controlUnit.getCurrentIn()));
        itLabel.setText(ControlUnit.getIt(controlUnit.getCurrentIt()));
    }

}
