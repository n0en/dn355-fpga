package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.Processor;

import javax.swing.*;
import java.awt.*;

public class ProcessorControlFrame extends AbstractDisplayFrame {

    private final transient Processor processor;

    private boolean run = false;
    private int delayTicks = 10;
    private int currentTicks;

    public ProcessorControlFrame(Processor processor) {
        super("Controls", "control", 80, 80);

        this.processor = processor;

        initComponentsLocal();
    }

    private void initComponentsLocal() {
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(actionEvent -> processor.reset());

        c.gridx = 0;
        c.gridy = 0;
        add(resetButton, c);

        JButton stepButton = new JButton("Step");
        stepButton.addActionListener(actionEvent -> processor.step());

        c.gridx = 1;
        c.gridy = 0;
        add(stepButton, c);

        JButton runButton = new JButton("Run");
        runButton.addActionListener(actionEvent -> {
            currentTicks = delayTicks;
            run = true;
        });

        c.gridx = 2;
        c.gridy = 0;
        add(runButton, c);

        JButton stopButton = new JButton("Stop");
        stopButton.addActionListener(actionEvent -> run = false);

        c.gridx = 3;
        c.gridy = 0;
        add(stopButton, c);

        JButton slowerButton = new JButton("Slower");
        slowerButton.addActionListener(actionEvent -> delayTicks++);

        c.gridx = 2;
        c.gridy = 1;
        add(slowerButton, c);

        JButton fasterButton = new JButton("Faster");
        fasterButton.addActionListener(actionEvent -> delayTicks = (delayTicks > 1 ? delayTicks -1 : delayTicks));

        c.gridx = 3;
        c.gridy = 1;
        add(fasterButton, c);

        Timer timer = new Timer(100, actionEvent -> {
            if (run) {
                currentTicks--;
                if (currentTicks <= 0) {
                    processor.step();
                    currentTicks = delayTicks;
                }
            }
        });

        timer.start();
    }

}
