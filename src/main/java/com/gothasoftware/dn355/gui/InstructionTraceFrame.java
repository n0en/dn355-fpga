package com.gothasoftware.dn355.gui;


import com.gothasoftware.dn355.processor.*;

import javax.swing.*;
import java.awt.*;

public class InstructionTraceFrame extends AbstractDisplayFrame implements RegisterListener {

    private final DefaultListModel<String> model = new DefaultListModel<>();
    private final transient InstructionDecodeRegister registerIDR;
    private final transient SimpleRegister registerICTEMP;

    public InstructionTraceFrame(Processor processor) {
        super("Instruction Trace", "instruction.trace", 60, 60);

        initComponentsLocal();

        registerIDR = processor.getControlUnit().getRegisterIDR();
        registerIDR.addListener(this);

        registerICTEMP = processor.getRegisterICTEMP();
    }

    private void initComponentsLocal() {

        JScrollPane scrollPane = new JScrollPane();
        JList<String> instructionList = new JList<>();

        instructionList.setModel(model);
        instructionList.setBackground(Color.getColor("#DDDDDD"));
        instructionList.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));

        scrollPane.setViewportView(instructionList);
        add(scrollPane, BorderLayout.CENTER);

        pack();

    }

    @Override
    public void registerValueChanged(long oldValue, long newValue) {

        String mnemonic;
        String tag;
        String characterAddress = "";
        switch (registerIDR.getGroup()) {
            case 0:
                mnemonic = OpCodes.findOpCode(registerIDR.getOpCode()).toString();
                switch (registerIDR.getTag()) {
                    case 0:
                        tag = "*";
                        break;
                    case 1:
                        tag = "1";
                        break;
                    case 2:
                        tag = "2";
                        break;
                    case 3:
                        tag = "3";
                        break;
                    default:
                        throw new IllegalArgumentException("Unknwon tag '" + registerIDR.getTag() + "'");
                }
                model.addElement(String.format("%05o: %-7s %03o,%s,%s", registerICTEMP.getValue(), mnemonic, registerIDR.getDisplacement(), tag, characterAddress));
                break;
            case 1:
                mnemonic = registerIDR.getGroup1OpCode().toString();
                model.addElement(String.format("%05o: %-7s %03o,%s", registerICTEMP.getValue(), mnemonic, registerIDR.getDisplacement(), characterAddress));
                break;
            case 2:
                mnemonic = registerIDR.getGroup2OpCode().toString();
                model.addElement(String.format("%05o: %-7s %02o", registerICTEMP.getValue(), mnemonic, registerIDR.getK()));
                break;
            default:
                mnemonic = "???";
                model.addElement(String.format("%05o: %-7s", registerICTEMP.getValue(), mnemonic));
                break;
        }

    }

}
