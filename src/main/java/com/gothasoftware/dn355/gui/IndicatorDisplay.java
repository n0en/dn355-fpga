package com.gothasoftware.dn355.gui;


import com.gothasoftware.dn355.processor.Indicator;
import com.gothasoftware.dn355.processor.IndicatorListener;

import javax.swing.*;
import java.awt.*;

public class IndicatorDisplay extends JButton implements IndicatorListener {

    IndicatorDisplay(Indicator indicator) {
        super(indicator.getName());

        indicator.addListener(this);

        Dimension dim = new Dimension(indicator.getName().length() * 50, 16);
        setMinimumSize(dim);
        setPreferredSize(dim);
    }

    @Override
    public void indicatorChanged(String name, boolean newValue) {
        setBackground(newValue ? Color.GREEN : Color.LIGHT_GRAY);
    }

}
