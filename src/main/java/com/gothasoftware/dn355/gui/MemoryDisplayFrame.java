package com.gothasoftware.dn355.gui;

import com.gothasoftware.dn355.processor.Clock;
import com.gothasoftware.dn355.processor.Memory;

import javax.swing.*;
import java.awt.*;

public class MemoryDisplayFrame extends AbstractDisplayFrame {

    private static final int MAX_ROWS = 16;

    private final transient Clock clock;
    private final transient Memory memory;

    public MemoryDisplayFrame(Memory memory, Clock clock) {
        super("Memory", "memory", 60, 60);

        this.clock = clock;
        this.memory = memory;

        initComponentsLocal();
    }

    private void initComponentsLocal() {
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        for (int row = 0; row < MAX_ROWS; row++) {
            c.gridx = 0;
            c.gridy = row;
            add(new JLabel(padLeft(Integer.toOctalString(row * 8), "0", 5) + ": "), c);
            for (int col = 0; col < 8; col++) {
                c.gridx = col + 1;
                c.gridy = row;
                add(new MemoryCell(memory, (row * 8) + col, clock), c);
            }
        }

        pack();
    }

    public static String padLeft(String value, String pad, int minLength) {
        StringBuilder valueBuilder = new StringBuilder(value);
        while (valueBuilder.length() < minLength) {
            valueBuilder.insert(0, pad);
        }
        return valueBuilder.toString();
    }
}
