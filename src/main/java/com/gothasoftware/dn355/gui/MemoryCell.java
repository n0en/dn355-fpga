package com.gothasoftware.dn355.gui;


import com.gothasoftware.dn355.processor.Clock;
import com.gothasoftware.dn355.processor.ClockListener;
import com.gothasoftware.dn355.processor.Memory;
import com.gothasoftware.dn355.processor.MemoryChangeListener;

import javax.swing.*;
import java.awt.*;

public class MemoryCell extends JTextField implements ClockListener, MemoryChangeListener {

    private static final Dimension CELL_DIM = new Dimension(60, 20);

    private final transient Memory memory;
    private int cellIndex;

    public MemoryCell(Memory memory, int cellIndex, Clock clock) {
        this.memory = memory;
        this.cellIndex = cellIndex;

        initComponent();

        clock.addListener(this);
        memory.addChangeListener(this);
    }

    private void initComponent() {
        setPreferredSize(CELL_DIM);
        setMinimumSize(CELL_DIM);
        setBackground(Color.WHITE);

        updateDisplay();
    }

    private void updateDisplay() {
        setText(MemoryDisplayFrame.padLeft(Long.toOctalString(memory.read(cellIndex)), "0", 6));
    }

    @Override
    public void memoryChanged(int address, long oldValue, long newValue) {
        if (address == cellIndex) {
            setToolTipText(MemoryDisplayFrame.padLeft(Long.toOctalString(oldValue), "0", 6));
            updateDisplay();
            setBackground(Color.GREEN);
        }
    }

    public void setCellIndex(int cellIndex) {
        this.cellIndex = cellIndex;
        setBackground(Color.WHITE);
        updateDisplay();
    }

    @Override
    public void clockPrePulseInit() {
        setBackground(Color.WHITE);
    }

    @Override
    public void clockRisingEdge() {
        // Don't care
    }

    @Override
    public void clockFallingEdge() {
        // Don't care
    }
}
